// swift-tools-version:4.0
import PackageDescription

let package = Package(
        name: "SwiftQueue",
        products: [
            .library(name: "SwiftQueue", targets: ["SwiftQueue"])
        ],
        dependencies: [
            .package(url: "https://github.com/lucas34/Reachability.swift.git", .upToNextMajor(from: "5.0.0")),
            .package(url: "https://github.com/realm/realm-cocoa.git", .upToNextMajor(from: "3.3.3"))
        ],
        targets: [
            .target(
                    name: "SwiftQueue",
                    dependencies: ["Reachability","RealmSwift"]),
            .testTarget(
                    name: "SwiftQueueTests",
                    dependencies: ["SwiftQueue"])
        ],
        swiftLanguageVersions: [3, 4]
)
